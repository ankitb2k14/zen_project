import React, { Component } from 'react'
import { ColumnsContainer } from '../';

class BoxesContainer extends Component {

  render(){
    var TotalRows = [];
    const { numberOfRows, numberOfColumns, gutterRow, gutterCol } = this.props;
   
    for (var i = 0; i < numberOfRows; i++) {
        const perColumns = numberOfColumns.split(',') || 0;
        TotalRows.push(<ColumnsContainer key={i} index={i} numberOfColumns={perColumns[i]} gutterRow={gutterRow} gutterCol={gutterCol} />);
    }
    return TotalRows;
  }
}

export default BoxesContainer;