import React, { Component } from 'react'
import './ColumnsContainer.css';

class ColumnsContainer extends Component {


  render() {
    let TotalColumns = [];
    const { numberOfColumns, gutterRow, gutterCol, index } = this.props;
    for (var i = 0; i < numberOfColumns; i++) {
        TotalColumns.push(<div className='box' key={i} style={{marginRight: gutterCol}}><p className="box-text">100px Height <br /> Box</p><span className="column-number">Column {i + 1}</span></div>);
    }
    
    return (
      <div className='box-container' style={{marginBottom: gutterRow}}>
        <span className="row-number">Row {index + 1}</span>
        { TotalColumns }
      </div>
    )
  }
}

export default ColumnsContainer;
