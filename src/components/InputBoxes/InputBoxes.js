import React, { Component } from 'react'
import './InputBoxes.css';

class InputBoxes extends Component {
    
  //Initial State
  state = {
      'no-of-rows': '',
      'columns-string': '',
      'gutter-row': '',
      'gutter-col': ''
  }

  /**
   * Handle Changes of Input
   *
   * @param {DOM} e
   * @createdOn - 17/1/2019
   * @author Ankit Bhatnagar
   */
  handleChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    const validity = (e.target.validity.valid) ? value : "";
    this.setState(() => ({ [name]: validity} ), 
    () => {
        this.props.onCallBack(this.state);
    })
  };

  render() {
    return (
      <div className="zen-input-flex">
        <input className="zen-input" name="no-of-rows" placeholder="No. of Rows (eg-3)" pattern="[0-9]*" value={this.state['no-of-rows']} onChange={this.handleChange} />
        <input className="zen-input" name="columns-string" placeholder="Column String (eg-20,10)" pattern="^(?=.*?[1-9])[0-9,]+$" value={this.state['columns-string']} onChange={this.handleChange} />
        <input className="zen-input" name="gutter-row" placeholder="Space between rows (eg-8px)" value={this.state['gutter-row']} onChange={this.handleChange} />
        <input className="zen-input" name="gutter-col" placeholder="Space between boxes (eg-8px)" value={this.state['gutter-col']}  onChange={this.handleChange} />
      </div>
    )
  }
}

export default InputBoxes;