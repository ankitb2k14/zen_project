import React, { Component } from 'react';
import { InputBoxes, BoxesContainer } from './components';
import './App.css';

class App extends Component {
  
  state = {
    inputStates: {
      'no-of-rows': 0,
      'columns-string': "0",
      'gutter-row': "",
      'gutter-col': ""
    }
  };

  /**
   * Check Input Changes
   *
   * @param {object} inputStates
   * @createdOn - 17/1/2019
   * @author Ankit Bhatnagar
   */
  inputBoxChanges = (inputStates) => {
    this.setState(() => ({ inputStates }));
  };

  render() {
    const { inputStates } = this.state;
    return (
      <div className="zen-container">
        <div className="zen-wrapper">
          <div className="zen-header">
            <h2>React Grid System</h2>
          </div>
          <div className="zen-body-container">
              <InputBoxes onCallBack={this.inputBoxChanges}/>
              <div className="zen-box-flex">
                <BoxesContainer 
                  numberOfRows={inputStates['no-of-rows']} 
                  numberOfColumns={inputStates['columns-string']}
                  gutterRow={inputStates['gutter-row']}
                  gutterCol={inputStates['gutter-col']}
                />
              </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
